# Resources

- [GamingOnLinux](https://www.gamingonlinux.com)

- [LibrePlanet Gaming Collective](https://libreplanet.org/wiki/Group:LibrePlanet_Gaming_Collective)

- [Libretro](https://www.libretro.com)

- [Linux Game Database](https://lgdb.org)

- [RetroArch](http://retroarch.com)
